#!/bin/sh

# Create spool directory
mkdir -p /var/spool/rsyslog

# Run RSyslog daemon
/usr/sbin/rsyslogd
nginx
exec /usr/local/sbin/php-fpm