FROM imcj/php-docker

RUN apt install rsyslog -y
RUN apt install nginx -y
RUN docker-php-ext-enable opcache
ADD run.sh /tmp/run.sh
RUN chmod +x /tmp/run.sh
ADD rsyslog.conf /etc/
ADD ./php-fpm.d  /usr/local/etc/php-fpm.d
ADD php/php.ini /usr/local/etc/php.ini
# ADD info.php /www/public/index.php
ADD ./nginx /etc/nginx/
CMD ["sh", "-c", "/tmp/run.sh"]